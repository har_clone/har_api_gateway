package grpc_client

import (
	"fmt"

	"gitlab.com/har_clone/har_api_gateway/config"
	pbb "gitlab.com/har_clone/har_api_gateway/genproto/broker_service"
	pbbg "gitlab.com/har_clone/har_api_gateway/genproto/brokers_group_service"
	pbc "gitlab.com/har_clone/har_api_gateway/genproto/comment_service"
	pbPay "gitlab.com/har_clone/har_api_gateway/genproto/payment_service"
	pbp "gitlab.com/har_clone/har_api_gateway/genproto/post_service"
	pbu "gitlab.com/har_clone/har_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	TransactionStatusPaid    = "paid"
	TransactionStatusPending = "pending"
	TransactionStatusFailed  = "failed"
)

type GrpcClientI interface {
	UserService() pbu.UserServiceClient
	// AuthService() pbu.AuthServiceClient TODO:
	BrokerService() pbb.BrokerServiceClient
	BrokersGroupService() pbbg.BrokersGroupServiceClient
	ApartmentService() pbp.ApartmentServiceClient
	CommentService() pbc.CommentServiceClient
	LikeService() pbc.LikeServiceClient
	PaymentService() pbPay.PaymentServiceClient
	CollectedApartmentsService() pbc.CollectedApartmentsServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.UserServiceHost, cfg.UserServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %s port:%s err: %v",
			cfg.UserServiceHost, cfg.UserServiceGrpcPort, err)
	}

	connBrokerService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.BrokerServiceHost, cfg.BrokerServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("broker service dial host: %s port:%s err: %v",
			cfg.UserServiceHost, cfg.UserServiceGrpcPort, err)
	}

	connPostService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApartmentServiceHost, cfg.ApartmentServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("apartment service dial host: %s port:%s err: %v",
			cfg.ApartmentServiceHost, cfg.ApartmentServiceGrpcPort, err)
	}

	connCommentService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CommentServiceHost, cfg.CommentServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("comment service dial host: %s port:%s err: %v",
			cfg.CommentServiceHost, cfg.CommentServiceGrpcPort, err)
	}
	connPaymentService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.PaymentServiceHost, cfg.PaymentServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("payment service dial host: %s port:%s err: %v",
			cfg.PaymentServiceHost, cfg.PaymentServiceGrpcPort, err)
	}
	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"payment_service":       pbPay.NewPaymentServiceClient(connPaymentService),
			"user_service":          pbu.NewUserServiceClient(connUserService),
			"broker_service":        pbb.NewBrokerServiceClient(connBrokerService),
			"brokers_group_service": pbbg.NewBrokersGroupServiceClient(connBrokerService),
			// "auth_user_service":     pbu.NewAuthServiceClient(connUserService), TODO:
			"apartment_service":            pbp.NewApartmentServiceClient(connPostService),
			"comment_service":              pbc.NewCommentServiceClient(connCommentService),
			"like_service":                 pbc.NewLikeServiceClient(connCommentService),
			"collected_apartments_service": pbc.NewCollectedApartmentsServiceClient(connCommentService),
		},
	}, nil
}

// func (g *GrpcClient) AuthService() pbu.AuthServiceClient {
// 	return g.connections["auth_user_service"].(pbu.AuthServiceClient) TODO:
// }

func (g *GrpcClient) PaymentService() pbPay.PaymentServiceClient {
	return g.connections["payment_service"].(pbPay.PaymentServiceClient)
}

func (g *GrpcClient) UserService() pbu.UserServiceClient {
	return g.connections["user_service"].(pbu.UserServiceClient)
}
func (g *GrpcClient) BrokerService() pbb.BrokerServiceClient {
	return g.connections["broker_service"].(pbb.BrokerServiceClient)
}

func (g *GrpcClient) BrokersGroupService() pbbg.BrokersGroupServiceClient {
	return g.connections["brokers_group_service"].(pbbg.BrokersGroupServiceClient)
}

func (g *GrpcClient) ApartmentService() pbp.ApartmentServiceClient {
	return g.connections["apartment_service"].(pbp.ApartmentServiceClient)
}

func (g *GrpcClient) CommentService() pbc.CommentServiceClient {
	return g.connections["comment_service"].(pbc.CommentServiceClient)
}

func (g *GrpcClient) LikeService() pbc.LikeServiceClient {
	return g.connections["like_service"].(pbc.LikeServiceClient)
}

func (g *GrpcClient) CollectedApartmentsService() pbc.CollectedApartmentsServiceClient {
	return g.connections["collected_apartments_service"].(pbc.CollectedApartmentsServiceClient)
}
