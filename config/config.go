package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	HttpPort                 string
	UserServiceGrpcPort      string
	UserServiceHost          string
	BrokerServiceGrpcPort    string
	BrokerServiceHost        string
	ApartmentServiceGrpcPort string
	ApartmentServiceHost     string
	CommentServiceGrpcPort   string
	CommentServiceHost       string
	PaymentServiceGrpcPort   string
	PaymentServiceHost       string
	AuthSecretKey            string
	Stripe                   Stripe
}
type Stripe struct {
	SecretKey      string
	EndpointSecret string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		HttpPort:                 conf.GetString("HTTP_PORT"),
		UserServiceHost:          conf.GetString("USER_SERVICE_HOST"),
		UserServiceGrpcPort:      conf.GetString("USER_SERVICE_GRPC_PORT"),
		BrokerServiceHost:        conf.GetString("BROKER_SERVICE_HOST"),
		BrokerServiceGrpcPort:    conf.GetString("BROKER_SERVICE_GRPC_PORT"),
		ApartmentServiceHost:     conf.GetString("APARTMENT_SERVICE_HOST"),
		ApartmentServiceGrpcPort: conf.GetString("APARTMENT_SERVICE_GRPC_PORT"),
		CommentServiceHost:       conf.GetString("COMMENT_SERVICE_HOST"),
		CommentServiceGrpcPort:   conf.GetString("COMMENT_SERVICE_GRPC_PORT"),
		PaymentServiceHost:       conf.GetString("PAYMENT_SERVICE_HOST"),
		PaymentServiceGrpcPort:   conf.GetString("PAYMENT_SERVICE_GRPC_PORT"),
		AuthSecretKey:            conf.GetString("AUTH_SECRET_KEY"),
		Stripe: Stripe{
			SecretKey:      conf.GetString("STRIPE_SECRET_KEY"),
			EndpointSecret: conf.GetString("STRIPE_ENDPOINT_SECRET"),
		},
	}

	return cfg
}
