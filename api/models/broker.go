package models

type Broker struct {
	Id              int64   `json:"id"`
	FirstName       string  `json:"first_name" binding:"required,min=2,max=30"`
	LastName        string  `json:"last_name" binding:"required,min=2,max=30"`
	Username        string  `json:"username"`
	Password        string  `json:"password" binding:"required,min=6,max=16"`
	Bio             string  `json:"bio"`
	ProfileImageUrl string  `json:"profile_image_url"`
	Email           string  `json:"email" binding:"required,email"`
	Gender          string  `json:"gender" binding:"oneof=male female"`
	Type            string  `json:"type" binding:"oneof=user broker superadmin"`
	PhoneNumber     string  `json:"phone_number"`
	BrokerGroupId   int64   `json:"broker_group_id"`
	Rating          float32 `json:"rating"`
	Level           string  `json:"level"`
	CardId          int     `json:"card_id"`
	CreatedAt       string  `json:"created_at"`
}

type BrokerResponse struct {
	Id               int64            `json:"id"`
	FirstName        string           `json:"first_name" binding:"required,min=2,max=30"`
	LastName         string           `json:"last_name" binding:"required,min=2,max=30"`
	Username         string           `json:"username"`
	ProfileImageUrl  string           `json:"profile_image_url"`
	Bio              string           `json:"bio"`
	Email            string           `json:"email" binding:"required,email"`
	Gender           string           `json:"gender" binding:"oneof=male female"`
	PhoneNumber      string           `json:"phone_number"`
	Type             string           `json:"type" binding:"oneof=user broker superadmin"`
	BrokersGroupInfo BrokersGroupInfo `json:"brokers_group_info"`
	Rating           float32          `json:"rating"`
	Level            string           `json:"level"`
	CardId           int              `json:"card_id"`
	CreatedAt        string           `json:"created_at"`
}

type BrokersGroupInfo struct {
	Id          int64  `json:"id"`
	Name        string `json:"name"`
	GroupInfo   string `json:"group_info"`
	PhoneNumber string `json:"phone_number"`
	Email       string `json:"email"`
	Address     string `json:"address"`
	ImageUrl    string `json:"image_url"`
	Level       string `json:"level"`
	CreatedAt   string `json:"created_at"`
}

type CreateBrokerRequest struct {
	FirstName   string `json:"first_name" binding:"required,min=2,max=30"`
	LastName    string `json:"last_name" binding:"required,min=2,max=30"`
	Username    string `json:"username"`
	Bio         string `json:"bio"`
	Password    string `json:"password" binding:"required,min=6,max=16"`
	Email       string `json:"email" binding:"required,email"`
	PhoneNumber string `json:"phone_number"`
	Gender      string `json:"gender" binding:"oneof=male female"`
}

type UpdateBrokerRequest struct {
	FirstName   string `json:"first_name" binding:"required,min=2,max=30"`
	LastName    string `json:"last_name" binding:"required,min=2,max=30"`
	Username    string `json:"username"`
	PhoneNumber string `json:"phone_number"`
	Bio         string `json:"bio"`
	Gender      string `json:"gender" binding:"oneof=male female"`
}

type ResponseBroker struct {
	ID              int64   `json:"id"`
	FirstName       string  `json:"first_name" binding:"required,min=2,max=30"`
	LastName        string  `json:"last_name" binding:"required,min=2,max=30"`
	Username        string  `json:"username"`
	ProfileImageUrl string  `json:"profile_image_url"`
	Bio             string  `json:"bio"`
	Type            string  `json:"type" binding:"oneof=user broker superadmin"`
	Email           string  `json:"email" binding:"required,email"`
	Gender          string  `json:"gender" binding:"oneof=male female"`
	PhoneNumber     string  `json:"phone_number"`
	BrokerGroupId   int64   `json:"broker_group_id"`
	Rating          float32 `json:"rating"`
	Level           string  `json:"level"`
	CardId          int     `json:"card_id"`
	CreatedAt       string  `json:"created_at"`
}

type GetAllBrokersResponse struct {
	Brokers []*ResponseBroker `json:"brokers"`
	Count   int64             `json:"count"`
}

type GetAllBrokerParams struct {
	Limit      int32  `json:"limit" binding:"required" default:"10"`
	Page       int32  `json:"page" binding:"required" default:"1"`
	Search     string `json:"search"`
	SortByDate string `json:"sort_by_date" enums:"asc,desc" default:"desc"`
}

type SetBrokerLevel struct {
	Level string `json:"level" enums:"silver,gold,platinum" default:"silver"`
}

type AddCardReq struct {
	BrokerId int64 `json:"broker_id"`
	CardId   int64 `json:"card_id"`
}
