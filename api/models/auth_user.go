package models

type RegisterRequest struct {
	FirstName       string `json:"first_name" binding:"required,min=2,max=30"`
	LastName        string `json:"last_name" binding:"required,min=2,max=30"`
	Username        string `json:"username"`
	Password        string `json:"password" binding:"required,min=6,max=16"`
	ProfileImageUrl string `json:"profile_image_url"`
	Email           string `json:"email" binding:"required,email"`
	Type            string `json:"type" binding:"oneof=user broker superadmin"`
	Gender          string `json:"gender" binding:"oneof=male female"`
}

type AuthResponse struct {
	ID          int64  `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	Password    string `json:"password"`
	Username    string `json:"username"`
	Type            string `json:"type" binding:"oneof=user broker superadmin"`
	CreatedAt   string `json:"created_at"`
	AccessToken string `json:"access_token"`
}

type LoginRequest struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,min=6,max=16"`
}

type VerifyRequest struct {
	Email string `json:"email" binding:"required,email"`
	Code  string `json:"code" binding:"required"`
}

type ForgotPasswordRequest struct {
	Email string `json:"email" binding:"required,email"`
}

type UpdatePasswordRequest struct {
	Password string `json:"password" binding:"required"`
}
