package models

type BrokerInfo struct {
	Id              int64  `json:"id"`
	FirstName       string `json:"first_name"`
	LastName        string `json:"last_name"`
	Username        string `json:"username"`
	ProfileImageUrl string `json:"profile_image_url"`
	Email           string `json:"email"`
}

type BrokersGroupResponse struct {
	Id          int64      `json:"id"`
	Name        string     `json:"name"`
	AdminInfo   BrokerInfo `json:"admin_info"`
	GroupInfo   string     `json:"group_info"`
	PhoneNumber string     `json:"phone_number"`
	Email       string     `json:"emaail"`
	Address     string     `json:"address"`
	ImageUrl    string     `json:"image_url"`
	Level       string     `json:"level"`
	CreatedAt   string     `json:"created_at"`
}

type BrokersGroupImageUploadRequest struct {
	BrokersGroupId int64  `json:"brokers_group_id"`
	ImageUrl       string `json:"image_url"`
}

type BrokersGroup struct {
	Id          int64  `json:"id"`
	Name        string `json:"name"`
	AdminId     int64  `json:"admin_id"`
	GroupInfo   string `json:"group_info"`
	PhoneNumber string `json:"phone_number"`
	Email       string `json:"email"`
	Address     string `json:"address"`
	ImageUrl    string `json:"image_url"`
	Level       string `json:"level"`
	CreatedAt   string `json:"created_at"`
}

type ChangeBrokersGroup struct {
	Name        string `json:"name"`
	GroupInfo   string `json:"group_info"`
	PhoneNumber string `json:"phone_number"`
	Address     string `json:"address"`
}

type CreateBrokersGroupRequest struct {
	Name        string `json:"name"`
	AdminId     int64  `json:"admin_id"`
	GroupInfo   string `json:"group_info"`
	PhoneNumber string `json:"phone_number"`
	Email       string `json:"email"`
	Address     string `json:"address"`
}

type GetAllBrokersGroupsResponse struct {
	BrokersGroups []*BrokersGroup `json:"brokers_groups"`
	Count         int64           `json:"count"`
}

type GetAllBrokersGroupsRequest struct {
	Limit      int32  `json:"limit" binding:"required" default:"10"`
	Page       int32  `json:"page" binding:"required" default:"1"`
	Search     string `json:"search"`
	SortByDate string `json:"sort_by_date" enums:"asc,desc" default:"desc"`
}

type AddMemberRequest struct {
	GroupId  int64 `json:"group_id"`
	BrokerId int64 `json:"broker_id"`
}

type GetAllMembers struct {
	GroupInfo BrokersGroupResponse `json:"group_info"`
	Members   []*GroupMember       `json:"members"`
	Count     int64                `json:"count"`
}
type GroupMember struct {
	Id        int64  `json:"id"`
	GroupId   int64  `json:"group_id"`
	BrokerId  int64  `json:"broker_id"`
	CreatedAt string `json:"created_at"`
}
