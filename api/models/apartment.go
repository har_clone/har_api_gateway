package models

type Apartment struct {
	Id            int64      `json:"id"`
	Title         string     `json:"title" binding:"required"`
	Description   string     `json:"description"`
	ApartmentType string     `json:"apartment_type" binding:"required"`
	Price         float64    `json:"price"`
	CategoryId    int64      `json:"category_id"`
	CategoryTitle string     `json:"category_title"`
	Address       string     `json:"address" binding:"required"`
	RoomsCount    int64      `json:"rooms_count"`
	AreaLength    float64    `json:"area_length"`
	Condition     string     `json:"condition" binding:"required"`
	ViewsCount    int64      `json:"views_count"`
	CreatedAt     string     `json:"created_at"`
	UpdatedAt     string     `json:"updated_at"`
	DeletedAt     string     `json:"deleted_at"`
	Holder        HolderInfo `json:"holder"`
}

type HolderInfo struct {
	Id              int64   `json:"id"`
	FirstName       string  `json:"first_name" binding:"required,min=2,max=30"`
	LastName        string  `json:"last_name" binding:"required,min=2,max=30"`
	Username        string  `json:"username"`
	ProfileImageUrl string  `json:"profile_image_url"`
	Email           string  `json:"email" binding:"required,email"`
	PhoneNumber     string  `json:"phone_number"`
	BrokerGroupId   int64   `json:"broker_group_id"`
	Rating          float32 `json:"rating"`
	Level           string  `json:"level"`
	CreatedAt       string  `json:"created_at"`
}

type GenerateApartment struct {
	HolderId      int64   `json:"holder_id"`
	HolderType    string  `json:"holder_type"`
	Title         string  `json:"title" binding:"required"`
	Description   string  `json:"description"`
	ApartmentType string  `json:"apartment_type" binding:"required"`
	Price         float64 `json:"price"`
	CategoryId    int64   `json:"category_id"`
	Address       string  `json:"address" binding:"required"`
	RoomsCount    int64   `json:"rooms_count"`
	AreaLength    float64 `json:"area_length"`
}

type ChangeApartment struct {
	Title         string  `json:"title" binding:"required"`
	Description   string  `json:"description"`
	Price         float64 `json:"price"`
	Address       string  `json:"address" binding:"required"`
	RoomsCount    int64   `json:"rooms_count"`
	AreaLength    float64 `json:"area_length"`
	ApartmentType string  `json:"apartment_type" binding:"required"`
}

type GetApartmentResponse struct {
	Apartments []*Apartment `json:"apartments"`
	Count      int64        `json:"count"`
}

type GetApartmentRequest struct {
	Limit         int32  `json:"limit" binding:"required" default:"10"`
	Page          int32  `json:"page" binding:"required" default:"1"`
	Search        string `json:"search"`
	SortByDate    string `json:"sort_by_date" enums:"asc,desc" default:"desc"`
	HolderId      int64  `json:"holder_id"`
	ApartmentType string `json:"apartment_type" enums:"rent,buy" default:""`
	Condition     string `json:"condition" enums:"active,sold" default:""`
	CategoryId    int64  `json:"category_id"`
}
