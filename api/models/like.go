package models

type Like struct {
	Id      int64 `json:"id"`
	GuestId int64 `json:"guest_id"`
	UserId  int64 `json:"user_id"`
	Status  bool  `json:"status"`
}

type GetLikeInfo struct {
	UserId   int64  `json:"user_id" binding:"required"`
	GuestId  int64  `json:"guest_id" binding:"required"`
	LikeType string `json:"like_type" binding:"required,oneof=apartment broker broker_group" default:"aparment"`
}

type IdLikeRequest struct {
	Id       int64  `json:"guest_id"`
	LikeType string `json:"like_type" binding:"required,oneof=apartment broker broker_group" default:"aparment"`
}

type LikesDislikesCountsResult struct {
	LikesCount    int64 `json:"like_count"`
	DislikesCount int64 `json:"dislike_count"`
}

type CreateOrUpdateLikeRequest struct {
	UserId   int64  `json:"user_id" binding:"required"`
	GuestId  int64  `json:"guest_id" binding:"required"`
	Status   bool   `json:"status" binding:"required,oneof=true false" default:"true"`
	LikeType string `json:"like_type" binding:"required,oneof=apartment broker broker_group" default:"aparment"`
}
