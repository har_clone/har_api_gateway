package models

type CollectedApartment struct {
	Id          int64  `json:"id"`
	UserId      int64  `json:"user_id"`
	ApartmentId int64  `json:"apartment_id"`
	CreatedAt   string `json:"created_at"`
}

type AddApartmentReq struct {
	UserId      int64 `json:"user_id"`
	ApartmentId int64 `json:"apartment_id"`
}

type GetAllCollectedApartmentsReq struct {
	Page       int64  `json:"page" binding:"required" default:"1"`
	Limit      int64  `json:"limit" binding:"required" default:"10"`
	UserId     int64  `json:"user_id"`
	SortByDate string `json:"sort_by_date" binding:"required,oneof=asc desc" default:"desc"`
}

type GetAllCollectedApartmentsRes struct {
	Apartments []*CollectedApartment `json:"apartments"`
	Count      int64                 `json:"count"`
}
