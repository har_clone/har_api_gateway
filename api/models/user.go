package models

type User struct {
	Id              int64  `json:"id"`
	FirstName       string `json:"first_name" binding:"required,min=2,max=30"`
	LastName        string `json:"last_name" binding:"required,min=2,max=30"`
	Username        string `json:"username"`
	Password        string `json:"password" binding:"required,min=6,max=16"`
	Bio             string `json:"bio"`
	ProfileImageUrl string `json:"profile_image_url"`
	Email           string `json:"email" binding:"required,email"`
	Gender          string `json:"gender" binding:"required"`
	Type            string `json:"type" binding:"oneof=user broker superadmin"`
	PhoneNumber     string `json:"phone_number"`
	CreatedAt       string `json:"created_at"`
}

type CreateUserRequest struct {
	FirstName       string `json:"first_name" binding:"required,min=2,max=30"`
	LastName        string `json:"last_name" binding:"required,min=2,max=30"`
	Username        string `json:"username"`
	Password        string `json:"password" binding:"required,min=6,max=16"`
	Email           string `json:"email" binding:"required,email"`
	PhoneNumber     string `json:"phone_number"`
	Gender          string `json:"gender" enums:"male,female" default:"male"`
	Bio             string `json:"bio"`
}

type UpdateUserRequest struct {
	FirstName       string `json:"first_name" binding:"required,min=2,max=30"`
	LastName        string `json:"last_name" binding:"required,min=2,max=30"`
	Username        string `json:"username"`
	PhoneNumber     string `json:"phone_number"`
	Gender          string `json:"gender" binding:"required"`
	Bio             string `json:"bio"`
}

type ResponseUser struct {
	ID              int64  `json:"id"`
	FirstName       string `json:"first_name" binding:"required,min=2,max=30"`
	LastName        string `json:"last_name" binding:"required,min=2,max=30"`
	Username        string `json:"username"`
	ProfileImageUrl string `json:"profile_image_url"`
	Bio             string `json:"bio"`
	Type            string `json:"type" binding:"oneof=user broker superadmin"`
	Email           string `json:"email" binding:"required,email"`
	Gender          string `json:"gender" binding:"required"`
	PhoneNumber     string `json:"phone_number"`
	CreatedAt       string `json:"created_at"`
}

type GetAllUsersResponse struct {
	Users []*ResponseUser `json:"users"`
	Count int64           `json:"count"`
}

type GetAllUserParams struct {
	Limit      int32  `json:"limit" binding:"required" default:"10"`
	Page       int32  `json:"page" binding:"required" default:"1"`
	Search     string `json:"search"`
	SortByDate string `json:"sort_by_date" enums:"asc,desc" default:"desc"`
}
