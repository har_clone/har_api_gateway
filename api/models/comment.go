package models

type Comment struct {
	Id          int    `json:"id" db:"id"`
	CommentType string `json:"comment_type"`
	GuestId     int    `json:"guest_id" db:"guest_id"`
	UserId      int    `json:"user_id" db:"user_id"`
	Description string `json:"description" db:"description"`
	CreatedAt   string `json:"created_at" db:"created_at"`
}

type CreateComment struct {
	UserId      int64  `json:"user_id"`
	CommentType string `json:"comment_type"`
	GuestId     int    `json:"guest_id" db:"guest_id"`
	Description string `json:"description" db:"description"`
}

type ChangeComment struct {
	UserId      int64  `json:"user_id"`
	Description string `json:"description" db:"description"`
}

type GetAllCommentsParams struct {
	Limit       int    `json:"limit" binding:"required" default:"10"`
	Page        int    `json:"page" binding:"required" default:"1"`
	UserId      int    `json:"user_id"`
	Search      string `json:"search"`
	CommentType string `json:"comment_type" binding:"required,oneof=apartment broker" default:"aparment"`
	GuestId     int    `json:"guest_id"`
	SortByDate  string `json:"sort_by_date" binding:"required,oneof=asc desc" default:"desc"`
}

type GetAllCommentsResponse struct {
	Comments []*Comment `json:"comments"`
	Count    int        `json:"count"`
}
