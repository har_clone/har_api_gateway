package v1

// import (
// 	"context"
// 	"net/http"
// 	"strconv"

// 	"github.com/gin-gonic/gin"
// 	"gitlab.com/har_clone/har_api_gateway/api/models"
// 	pbp "gitlab.com/har_clone/har_api_gateway/genproto/post_service"
// 	"google.golang.org/grpc/codes"
// 	"google.golang.org/grpc/status"
// )

// func parseApartmentModel(apartment *pbp.Apartment) *models.Apartment {
// 	return &models.Apartment{
// 		Id: apartment.Id,
// 		Holder: models.HolderInfo{
// 			Id:              apartment.Holder.Id,
// 			FirstName:       apartment.Holder.FirstName,
// 			LastName:        apartment.Holder.LastName,
// 			Username:        apartment.Holder.Username,
// 			ProfileImageUrl: apartment.Holder.ProfileImageUrl,
// 			Email:           apartment.Holder.Email,
// 			PhoneNumber:     apartment.Holder.PhoneNumber,
// 			BrokerGroupId:   apartment.Holder.BrokerGroupId,
// 			Rating:          apartment.Holder.Rating,
// 			Level:           apartment.Holder.Level,
// 			CreatedAt:       apartment.Holder.CreatedAt,
// 		},
// 		Title:         apartment.Title,
// 		Description:   apartment.Description,
// 		ApartmentType: apartment.ApartmentType,
// 		Price:         float64(apartment.Price),
// 		CategoryId:    apartment.CategoryId,
// 		CategoryTitle: apartment.CategoryTitle,
// 		Address:       apartment.Address,
// 		RoomsCount:    apartment.RoomsCount,
// 		AreaLength:    float64(apartment.AreaLength),
// 		Condition:     apartment.Condition,
// 		ViewsCount:    apartment.ViewsCount,
// 		CreatedAt:     apartment.CreatedAt,
// 		UpdatedAt:     apartment.UpdatedAt,
// 	}
// }

// // @Router /apartments [post]
// // @Summary Create a apartment
// // @Description Create a apartment
// // @Tags apartment
// // @Accept json
// // @Produce json
// // @Param apartment body models.GenerateApartment true "apartment"
// // @Success 201 {object} models.Apartment
// // @Failure 500 {object} models.ErrorResponse
// func (h *handlerV1) ApartmentCreate(c *gin.Context) {
// 	var (
// 		req models.GenerateApartment
// 	)

// 	err := c.ShouldBindJSON(&req)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, errorResponse(err))
// 		return
// 	}

// 	resp, err := h.grpcClient.ApartmentService().CreateApartment(context.Background(), &pbp.GenerateApartment{
// 		HolderId:      req.HolderId,
// 		HolderType:    req.HolderType,
// 		Title:         req.Title,
// 		Description:   req.Description,
// 		ApartmentType: req.ApartmentType,
// 		Price:         float32(req.Price),
// 		CategoryId:    req.CategoryId,
// 		Address:       req.Address,
// 		RoomsCount:    req.RoomsCount,
// 		AreaLength:    float32(req.AreaLength),
// 	})
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, errorResponse(err))
// 		return
// 	}

// 	apartment := parseApartmentModel(resp)
// 	c.JSON(http.StatusCreated, apartment)
// }

// // @Router /apartments/{id} [get]
// // @Summary Get apartment by id
// // @Description Get apartment by id
// // @Tags apartment
// // @Accept json
// // @Produce json
// // @Param id path int true "ID"
// // @Success 200 {object} models.Apartment
// // @Failure 500 {object} models.ErrorResponse
// func (h *handlerV1) ApartmentGet(c *gin.Context) {
// 	id, err := strconv.Atoi(c.Param("id"))
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, models.ErrorResponse{
// 			Error: err.Error(),
// 		})
// 		return
// 	}

// 	resp, err := h.grpcClient.ApartmentService().GetApartment(context.Background(), &pbp.IdByRequestApartment{Id: int64(id)})
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, errorResponse(err))
// 		return
// 	}

// 	c.JSON(http.StatusOK, parseApartmentModel(resp))
// }

// // @Router /apartments [get]
// // @Summary Get all apartments
// // @Description Get all apartments
// // @Tags apartment
// // @Accept json
// // @Produce json
// // @Param filter query models.GetApartmentRequest false "Filter"
// // @Success 200 {object} models.GetApartmentResponse
// // @Failure 500 {object} models.ErrorResponse
// func (h *handlerV1) ApartmentGetAll(c *gin.Context) {
// 	req, err := apartmentParams(c)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, models.ErrorResponse{
// 			Error: err.Error(),
// 		})
// 		return
// 	}

// 	result, err := h.grpcClient.ApartmentService().GetAllApartmentRequest(context.Background(), &pbp.GetApartmentRequest{
// 		Page:          req.Page,
// 		Limit:         req.Limit,
// 		Search:        req.Search,
// 		HolderId:      req.HolderId,
// 		CategoryId:    req.CategoryId,
// 		SortedByDate:  req.SortByDate,
// 		ApartmentType: req.ApartmentType,
// 		Condition:     req.Condition,
// 	})
// 	if err != nil {
// 		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
// 			Error: err.Error(),
// 		})
// 	}

// 	var res models.GetApartmentResponse
// 	res.Count = result.Count
// 	for _, apartment := range result.Apartments {
// 		res.Apartments = append(res.Apartments, &models.Apartment{
// 			Id: apartment.Id,
// 			Holder: models.HolderInfo{
// 				Id:              apartment.Holder.Id,
// 				FirstName:       apartment.Holder.FirstName,
// 				LastName:        apartment.Holder.LastName,
// 				Username:        apartment.Holder.Username,
// 				ProfileImageUrl: apartment.Holder.ProfileImageUrl,
// 				Email:           apartment.Holder.Email,
// 				PhoneNumber:     apartment.Holder.PhoneNumber,
// 				BrokerGroupId:   apartment.Holder.BrokerGroupId,
// 				Rating:          apartment.Holder.Rating,
// 				Level:           apartment.Holder.Level,
// 				CreatedAt:       apartment.Holder.CreatedAt,
// 			},
// 			Title:         apartment.Title,
// 			Description:   apartment.Description,
// 			ApartmentType: apartment.ApartmentType,
// 			Price:         float64(apartment.Price),
// 			CategoryId:    apartment.CategoryId,
// 			CategoryTitle: apartment.ApartmentType,
// 			Address:       apartment.Address,
// 			RoomsCount:    apartment.RoomsCount,
// 			AreaLength:    float64(apartment.AreaLength),
// 			Condition:     apartment.Condition,
// 			ViewsCount:    apartment.ViewsCount,
// 			CreatedAt:     apartment.CreatedAt,
// 			UpdatedAt:     apartment.UpdatedAt,
// 			DeletedAt:     apartment.DeletedAt,
// 		})
// 	}
// 	if res.Apartments == nil {
// 		res.Apartments = []*models.Apartment{}
// 	}

// 	c.JSON(http.StatusOK, res)
// }

// func apartmentParams(c *gin.Context) (*models.GetApartmentRequest, error) {
// 	var (
// 		limit                int = 10
// 		page                 int = 1
// 		err                  error
// 		SortByDate           string
// 		ApartmentType        string
// 		Condition            string
// 		CategoryId, HolderId int
// 	)

// 	if c.Query("limit") != "" {
// 		limit, err = strconv.Atoi(c.Query("limit"))
// 		if err != nil {
// 			return nil, err
// 		}
// 	}

// 	if c.Query("page") != "" {
// 		page, err = strconv.Atoi(c.Query("page"))
// 		if err != nil {
// 			return nil, err
// 		}
// 	}

// 	if c.Query("category_id") != "" {
// 		CategoryId, err = strconv.Atoi(c.Query("category_id"))
// 		if err != nil {
// 			return nil, err
// 		}
// 	}

// 	if c.Query("holder_id") != "" {
// 		HolderId, err = strconv.Atoi(c.Query("holder_id"))
// 		if err != nil {
// 			return nil, err
// 		}
// 	}
// 	if c.Query("sort_by_date") != "" &&
// 		(c.Query("sort_by_date") == "desc" || c.Query("sort_by_date") == "asc" || c.Query("sort_by_date") == "none") {
// 		SortByDate = c.Query("sort_by_date")
// 	}

// 	if c.Query("condition") != "" &&
// 		(c.Query("condition") == "sold" || c.Query("condition") == "active" || c.Query("condition") == "none") {
// 		Condition = c.Query("condition")
// 	}
// 	if c.Query("apartment_type") != "" &&
// 		(c.Query("apartment_type") == "buy" || c.Query("apartment_type") == "rent" || c.Query("apartment_type") == "none") {
// 		ApartmentType = c.Query("apartment_type")
// 	}
// 	return &models.GetApartmentRequest{
// 		Limit:         int32(limit),
// 		Page:          int32(page),
// 		Search:        c.Query("search"),
// 		SortByDate:    SortByDate,
// 		Condition:     Condition,
// 		ApartmentType: ApartmentType,
// 		HolderId:      int64(HolderId),
// 		CategoryId:    int64(CategoryId),
// 	}, nil
// }

// // @Router /apartments/{id} [put]
// // @Summary Update apartment
// // @Description Update apartment
// // @Tags apartment
// // @Accept json
// // @Produce json
// // @Param id path int true "ID"
// // @Param apartment body models.ChangeApartment true "apartment"
// // @Success 201 {object} models.Apartment
// // @Failure 500 {object} models.ErrorResponse
// func (h *handlerV1) ApartmentUpdate(c *gin.Context) {
// 	var (
// 		req models.ChangeApartment
// 	)
// 	err := c.ShouldBindJSON(&req)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, errorResponse(err))
// 		return
// 	}

// 	id, err := strconv.Atoi(c.Param("id"))
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, errorResponse(err))
// 		return
// 	}

// 	resp, err := h.grpcClient.ApartmentService().UpdateApartment(context.Background(), &pbp.ChangeApartment{
// 		Id:            int64(id),
// 		Title:         req.Title,
// 		Description:   req.Description,
// 		Price:         float32(req.Price),
// 		Address:       req.Address,
// 		RoomsCount:    req.RoomsCount,
// 		AreaLength:    float32(req.AreaLength),
// 		ApartmentType: req.ApartmentType,
// 	})

// 	if err != nil {
// 		if s, _ := status.FromError(err); s.Code() == codes.NotFound {
// 			c.JSON(http.StatusNotFound, errorResponse(err))
// 			return
// 		}
// 		c.JSON(http.StatusInternalServerError, errorResponse(err))
// 		return
// 	}

// 	apartment := parseApartmentModel(resp)
// 	c.JSON(http.StatusCreated, apartment)
// }

// // @Summary Delete a apartments
// // @Description Delete a apartments
// // @Tags apartment
// // @Accept json
// // @Produce json
// // @Param id path int true "ID"
// // @Success 200 {object} models.Apartment
// // @Failure 500 {object} models.ErrorResponse
// // @Router /apartments/{id} [delete]
// func (h *handlerV1) ApartmentDelete(ctx *gin.Context) {
// 	id, err := strconv.Atoi(ctx.Param("id"))
// 	if err != nil {
// 		ctx.JSON(http.StatusInternalServerError, gin.H{
// 			"message": "failed to convert",
// 		})
// 		return
// 	}

// 	resp, err := h.grpcClient.ApartmentService().DeleteApartment(context.Background(), &pbp.IdByRequestApartment{
// 		Id: int64(id),
// 	})
// 	if err != nil {
// 		ctx.JSON(http.StatusInternalServerError, gin.H{
// 			"message": err.Error(),
// 		})
// 		return
// 	}
// 	ctx.JSON(http.StatusOK, gin.H{
// 		"message": "successful delete method",
// 	})

// 	apartment := parseApartmentModel(resp)
// 	ctx.JSON(http.StatusOK, apartment)
// }
