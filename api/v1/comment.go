package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/har_clone/har_api_gateway/api/models"
	pbc "gitlab.com/har_clone/har_api_gateway/genproto/comment_service"
)

// @Router /comments/get/{id} [get]
// @Summary Get comment by id
// @Description Get comment by id
// @Tags comments
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.Comment
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetComment(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	resp, err := h.grpcClient.CommentService().GetComment(context.Background(), &pbc.IdByCommentRequest{
		Id: int64(id),
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, models.Comment{
		Id:          int(resp.Id),
		GuestId:     int(resp.GuestId),
		UserId:      int(resp.UserId),
		CommentType: resp.CommentType,
		Description: resp.Description,
		CreatedAt:   resp.CreatedAt,
	})
}

// @Router /comments/create/ [post]
// @Summary Create a comment
// @Description Create a comment
// @Tags comments
// @Accept json
// @Produce json
// @Param comment body models.CreateComment true "comment"
// @Success 201 {object} models.Comment
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateComment(c *gin.Context) {
	var (
		req models.CreateComment
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	// payload, err := h.GetAuthPayload(c)
	// if err != nil {
	// 	c.JSON(http.StatusBadRequest, models.ErrorResponse{
	// 		Error: err.Error(),
	// 	})
	// 	return
	// }

	resp, err := h.grpcClient.CommentService().CreateComment(context.Background(), &pbc.CommentGenerate{
		GuestId:     int64(req.GuestId),
		CommentType: req.CommentType,
		UserId:      req.UserId,
		Description: req.Description,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, models.Comment{
		Id:          int(resp.Id),
		CommentType: resp.CommentType,
		GuestId:     int(resp.GuestId),
		UserId:      int(resp.UserId),
		Description: resp.Description,
		CreatedAt:   resp.CreatedAt,
	})
}

// @Router /comments/get-all [get]
// @Summary Get all comments
// @Description Get all comments
// @Tags comments
// @Accept json
// @Produce json
// @Param filter query models.GetAllCommentsParams false "Filter"
// @Success 200 {object} models.GetAllCommentsParams
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllComment(c *gin.Context) {
	req, err := commentsParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	result, err := h.grpcClient.CommentService().GetAllComments(context.Background(), &pbc.GetCommentRequest{
		Page:        int64(req.Page),
		Limit:       int64(req.Limit),
		GuestId:     int64(req.GuestId),
		UserId:      int64(req.UserId),
		Search:      req.Search,
		CommentType: req.CommentType,
		SortByDate:  req.SortByDate,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, commentsResponse(h, result))
}

func commentsParams(c *gin.Context) (*models.GetAllCommentsParams, error) {
	var (
		limit           int = 10
		page            int = 1
		err             error
		sortByDate      string
		GuestId, UserId int
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("sort_by_date") != "" &&
		(c.Query("sort_by_date") == "desc" || c.Query("sort_by_date") == "asc" || c.Query("sort_by_date") == "none") {
		sortByDate = c.Query("sort_by_date")
	}

	if c.Query("guest_id") != "" {
		GuestId, err = strconv.Atoi(c.Query("guest_id"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("user_id") != "" {
		UserId, err = strconv.Atoi(c.Query("user_id"))
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllCommentsParams{
		Limit:       limit,
		Page:        page,
		UserId:      UserId,
		Search:      c.Query("search"),
		CommentType: c.Query("comment_type"),
		GuestId:     GuestId,
		SortByDate:  sortByDate,
	}, nil
}

func commentsResponse(h *handlerV1, data *pbc.GetCommentResponse) *models.GetAllCommentsResponse {
	response := models.GetAllCommentsResponse{
		Comments: make([]*models.Comment, 0),
		Count:    int(data.Count),
	}

	for _, comment := range data.Comments {
		p := parseCommentModel(comment)
		response.Comments = append(response.Comments, &p)
	}

	return &response
}

func parseCommentModel(Comment *pbc.Comment) models.Comment {
	return models.Comment{
		Id:          int(Comment.Id),
		CommentType: Comment.CommentType,
		GuestId:     int(Comment.GuestId),
		UserId:      int(Comment.UserId),
		Description: Comment.Description,
		CreatedAt:   Comment.CreatedAt,
	}
}

// @Router /comments/update/{id} [put]
// @Summary Update a comment
// @Description Update a comments
// @Tags comments
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param comment body models.ChangeComment true "comment"
// @Success 200 {object} models.Comment
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateComment(ctx *gin.Context) {
	var req models.ChangeComment
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	// payload, err := h.GetAuthPayload(ctx)
	// if err != nil {
	// 	ctx.JSON(http.StatusBadRequest, models.ErrorResponse{
	// 		Error: err.Error(),
	// 	})
	// 	return
	// }

	comment, err := h.grpcClient.CommentService().UpdateComment(context.Background(), &pbc.ChangeComment{
		Id:          int64(id),
		Description: req.Description,
		UserId:      req.UserId,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, models.Comment{
		Id:          int(comment.Id),
		GuestId:     int(comment.GuestId),
		UserId:      int(req.UserId),
		Description: comment.Description,
		CreatedAt:   comment.CreatedAt,
	})
}

// @Router /comments/delete/{id} [delete]
// @Summary Delete a comment
// @Description Delete a comment
// @Tags comments
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteComment(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})
		return
	}

	_, err = h.grpcClient.CommentService().DeleteComment(context.Background(), &pbc.IdByCommentRequest{Id: int64(id)})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to Delete method",
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "successful delete method",
	})
}
