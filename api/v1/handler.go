package v1

import (
	"errors"

	"gitlab.com/har_clone/har_api_gateway/api/models"
	"gitlab.com/har_clone/har_api_gateway/config"
	grpcPkg "gitlab.com/har_clone/har_api_gateway/pkg/grpc_client"
)

var (
	ErrWrongEmailOrPass = errors.New("wrong email or password")
	ErrEmailExists      = errors.New("email already exists")
	ErrUserNotVerified  = errors.New("user not verified")
	ErrIncorrectCode    = errors.New("incorrect verification code")
	ErrCodeExpired      = errors.New("verification code has been expired")
	ErrNotAllowed       = errors.New("method not allowed")
	ErrForbidden        = errors.New("forbidden")
)

type handlerV1 struct {
	cfg        *config.Config
	grpcClient grpcPkg.GrpcClientI
}

type HandlerV1Options struct {
	Cfg        *config.Config
	GrpcClient *grpcPkg.GrpcClientI
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:        options.Cfg,
		grpcClient: *options.GrpcClient,
	}
}

func errorResponse(err error) *models.ErrorResponse {
	return &models.ErrorResponse{
		Error: err.Error(),
	}
}
