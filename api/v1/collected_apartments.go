package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/har_clone/har_api_gateway/api/models"
	pbc "gitlab.com/har_clone/har_api_gateway/genproto/comment_service"
)

// @Router /collected-apartments/add/ [post]
// @Summary collect apartment
// @Description collect apartment
// @Tags collected apartment
// @Accept json
// @Produce json
// @Param apartment body models.AddApartmentReq true "apartment"
// @Success 201 {object} models.CollectedApartment
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) AddCollectApartment(c *gin.Context) {
	var (
		req models.AddApartmentReq
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	// TODO: MiddleWare qo'y ovshar

	resp, err := h.grpcClient.CollectedApartmentsService().AddApartment(context.Background(), &pbc.AddApartmentReq{
		UserId:      req.UserId,
		ApartmentId: req.ApartmentId,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, models.CollectedApartment{
		Id:          resp.Id,
		UserId:      resp.UserId,
		ApartmentId: resp.ApartmentId,
		CreatedAt:   resp.CreatedAt,
	})
}

// @Router /collected-apartments/get/{id} [get]
// @Summary Get Collected Apartments
// @Description Get Collected Apartments
// @Tags collected apartment
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.Comment
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetCollectedApartment(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	resp, err := h.grpcClient.CollectedApartmentsService().GetApartment(context.Background(), &pbc.GetApartmentIdRequest{
		Id: int64(id),
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, models.CollectedApartment{
		Id:          resp.Id,
		UserId:      resp.UserId,
		ApartmentId: resp.ApartmentId,
		CreatedAt:   resp.CreatedAt,
	})
}

// @Router /collected-apartments/get-all [get]
// @Summary Get all collected apartments
// @Description Get all collected apartments
// @Tags collected apartment
// @Accept json
// @Produce json
// @Param filter query models.GetAllCollectedApartmentsReq false "Filter"
// @Success 200 {object} models.GetAllCollectedApartmentsRes
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllCollectedApartments(c *gin.Context) {
	req, err := collectedApartmentsParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	result, err := h.grpcClient.CollectedApartmentsService().GetAllCollectedApartments(context.Background(), &pbc.GetAllCollectedApartmentsReq{
		Page:       int64(req.Page),
		Limit:      int64(req.Limit),
		UserId:     int64(req.UserId),
		SortByDate: req.SortByDate,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, collectedApartmentsResponse(h, result))
}

func collectedApartmentsParams(c *gin.Context) (*models.GetAllCollectedApartmentsReq, error) {
	var (
		limit      int = 10
		page       int = 1
		err        error
		sortByDate string
		UserId     int
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("sort_by_date") != "" &&
		(c.Query("sort_by_date") == "desc" || c.Query("sort_by_date") == "asc" || c.Query("sort_by_date") == "none") {
		sortByDate = c.Query("sort_by_date")
	}

	if c.Query("user_id") != "" {
		UserId, err = strconv.Atoi(c.Query("user_id"))
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllCollectedApartmentsReq{
		Limit:      int64(limit),
		Page:       int64(page),
		UserId:     int64(UserId),
		SortByDate: sortByDate,
	}, nil
}

func collectedApartmentsResponse(h *handlerV1, data *pbc.GetAllCollectedApartmentsRes) *models.GetAllCollectedApartmentsRes {
	response := models.GetAllCollectedApartmentsRes{
		Apartments: make([]*models.CollectedApartment, 0),
		Count:      data.Count,
	}

	for _, apartment := range data.Apartments {
		p := parseCollectedApartmentsModel(apartment)
		response.Apartments = append(response.Apartments, &p)
	}

	return &response
}

func parseCollectedApartmentsModel(req *pbc.CollectedApartment) models.CollectedApartment {
	return models.CollectedApartment{
		Id:          req.Id,
		UserId:      req.UserId,
		ApartmentId: req.ApartmentId,
		CreatedAt:   req.CreatedAt,
	}
}

// @Router /collected-apartments/delete/{id} [delete]
// @Summary Delete a apartment
// @Description Delete a apartment
// @Tags collected apartment
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteCollectedApartment(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})
		return
	}

	_, err = h.grpcClient.CollectedApartmentsService().DeleteApartment(context.Background(), &pbc.GetApartmentIdRequest{Id: int64(id)})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to Delete method",
		})
		return
	}
	ctx.JSON(http.StatusOK, models.ResponseOK{
		Message: "successful delete method",
	})
}
