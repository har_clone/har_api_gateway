package v1

import (
	"context"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/har_clone/har_api_gateway/api/models"
	pbbg "gitlab.com/har_clone/har_api_gateway/genproto/brokers_group_service"
)

type File struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

// @Router /brokers-group/create [post]
// @Summary Create a brokers group
// @Description Create a brokers group
// @Tags brokers group
// @Accept json
// @Produce json
// @Param broker body models.CreateBrokersGroupRequest true "Broker"
// @Success 201 {object} models.BrokersGroupResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateBrokersGroup(c *gin.Context) {
	var (
		req models.CreateBrokersGroupRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	brokersGroup, err := h.grpcClient.BrokersGroupService().CreateBrokersGroup(context.Background(), &pbbg.CreateBrokersGroupRequest{
		Name:        req.Name,
		AdminId:     req.AdminId,
		GroupInfo:   req.GroupInfo,
		PhoneNumber: req.PhoneNumber,
		Email:       req.Email,
		Address:     req.Address,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.BrokersGroupResponse{
		Id:        brokersGroup.Id,
		Name:      brokersGroup.Name,
		GroupInfo: brokersGroup.GroupInfo,
		AdminInfo: models.BrokerInfo{
			Id:              brokersGroup.AdminInfo.Id,
			FirstName:       brokersGroup.AdminInfo.FirstName,
			LastName:        brokersGroup.AdminInfo.LastName,
			Username:        brokersGroup.AdminInfo.Username,
			ProfileImageUrl: brokersGroup.AdminInfo.ProfileImageUrl,
			Email:           brokersGroup.AdminInfo.Email,
		},
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt,
	})
}

// @Router /brokers-group/add-member [post]
// @Summary add member
// @Description add member
// @Tags brokers group
// @Accept json
// @Produce json
// @Param broker body models.AddMemberRequest true "Broker"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) AddMember(c *gin.Context) {
	var (
		req models.AddMemberRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	_, err = h.grpcClient.BrokersGroupService().AddMember(context.Background(), &pbbg.AddMemberRequest{
		GroupId:  req.GroupId,
		BrokerId: req.BrokerId,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.ResponseOK{
		Message: "Added Member",
	})
}

// @Router /brokers-group/get-members/{id} [get]
// @Summary get member
// @Description get member
// @Tags brokers group
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.GetAllMembers
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetMembers(c *gin.Context) {
	var result models.GetAllMembers
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	response, err := h.grpcClient.BrokersGroupService().GetMembers(context.Background(), &pbbg.BrokersGroupIdRequest{
		Id: int64(id),
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	for _, i := range response.Members {
		result.Members = append(result.Members, &models.GroupMember{
			Id:        i.Id,
			GroupId:   i.GroupId,
			BrokerId:  i.BrokerId,
			CreatedAt: i.CreatedAt,
		})
	}
	result.Count = response.Count
	result.GroupInfo = models.BrokersGroupResponse{
		Id:          response.GroupInfo.Id,
		Name:        response.GroupInfo.Name,
		GroupInfo:   response.GroupInfo.GroupInfo,
		PhoneNumber: response.GroupInfo.PhoneNumber,
		Email:       response.GroupInfo.Email,
		Address:     response.GroupInfo.Address,
		ImageUrl:    response.GroupInfo.ImageUrl,
		Level:       response.GroupInfo.Level,
		CreatedAt:   response.GroupInfo.CreatedAt,
		AdminInfo: models.BrokerInfo{
			Id:              response.GroupInfo.AdminInfo.Id,
			FirstName:       response.GroupInfo.AdminInfo.FirstName,
			LastName:        response.GroupInfo.AdminInfo.LastName,
			Username:        response.GroupInfo.AdminInfo.Username,
			ProfileImageUrl: response.GroupInfo.AdminInfo.ProfileImageUrl,
			Email:           response.GroupInfo.AdminInfo.Email,
		},
	}

	c.JSON(http.StatusOK, result)
}

// @Router /brokers-group/get/{id} [get]
// @Summary Get brokersGroup by id
// @Description Get brokersGroup by id
// @Tags brokers group
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.BrokersGroupResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetBrokersGroup(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	brokersGroup, err := h.grpcClient.BrokersGroupService().GetBrokersGroup(context.Background(), &pbbg.BrokersGroupIdRequest{Id: int64(id)})

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, models.BrokersGroupResponse{
		Id:        brokersGroup.Id,
		Name:      brokersGroup.Name,
		GroupInfo: brokersGroup.GroupInfo,
		AdminInfo: models.BrokerInfo{
			Id:              brokersGroup.AdminInfo.Id,
			FirstName:       brokersGroup.AdminInfo.FirstName,
			LastName:        brokersGroup.AdminInfo.LastName,
			Username:        brokersGroup.AdminInfo.Username,
			ProfileImageUrl: brokersGroup.AdminInfo.ProfileImageUrl,
			Email:           brokersGroup.AdminInfo.Email,
		},
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt,
	})
}

// @Router /brokers-group/get-all [get]
// @Summary Get all brokers Groups
// @Description Get all brokers Groups
// @Tags brokers group
// @Accept json
// @Produce json
// @Param filter query models.GetAllBrokersGroupsRequest false "Filter"
// @Success 200 {object} models.GetAllBrokersGroupsResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllBrokersGroups(c *gin.Context) {
	req, err := validateGetAllParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.BrokersGroupService().GetAllBrokersGroups(context.Background(), &pbbg.GetAllBrokersGroupsRequest{
		Page:       int64(req.Page),
		Limit:      int64(req.Limit),
		Search:     req.Search,
		SortByDate: req.SortByDate,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, getBrokersGroupResponse(result))
}

func getBrokersGroupResponse(data *pbbg.GetAllBrokersGroupsResponse) *models.GetAllBrokersGroupsResponse {
	response := models.GetAllBrokersGroupsResponse{
		BrokersGroups: make([]*models.BrokersGroup, 0),
		Count:         data.Count,
	}

	for _, brokersGroup := range data.BrokersGroups {
		response.BrokersGroups = append(response.BrokersGroups, &models.BrokersGroup{
			Id:          brokersGroup.Id,
			Name:        brokersGroup.Name,
			AdminId:     brokersGroup.AdminId,
			GroupInfo:   brokersGroup.GroupInfo,
			PhoneNumber: brokersGroup.PhoneNumber,
			Email:       brokersGroup.Email,
			Address:     brokersGroup.Address,
			ImageUrl:    brokersGroup.ImageUrl,
			Level:       brokersGroup.ImageUrl,
			CreatedAt:   brokersGroup.ImageUrl,
		})
	}

	return &response
}

// @Router /brokers-group/update/{id} [put]
// @Summary Update a brokers group
// @Description Update a brokers group
// @Tags brokers group
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param broker body models.ChangeBrokersGroup true "broker"
// @Success 200 {object} models.BrokersGroupResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateBrokersGroup(ctx *gin.Context) {
	var (
		req models.ChangeBrokersGroup
	)

	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	brokersGroup, err := h.grpcClient.BrokersGroupService().UpdateBrokersGroup(context.Background(), &pbbg.ChangeBrokersGroup{
		Id:          int64(id),
		Name:        req.Name,
		GroupInfo:   req.GroupInfo,
		PhoneNumber: req.PhoneNumber,
		Address:     req.Address,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, models.BrokersGroupResponse{
		Id:        brokersGroup.Id,
		Name:      brokersGroup.Name,
		GroupInfo: brokersGroup.GroupInfo,
		AdminInfo: models.BrokerInfo{
			Id:              brokersGroup.AdminInfo.Id,
			FirstName:       brokersGroup.AdminInfo.FirstName,
			LastName:        brokersGroup.AdminInfo.LastName,
			Username:        brokersGroup.AdminInfo.Username,
			ProfileImageUrl: brokersGroup.AdminInfo.ProfileImageUrl,
			Email:           brokersGroup.AdminInfo.Email,
		},
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt,
	})
}

// @Router /brokers-group/image-upload/{id} [post]
// @Summary File image upload
// @Description File image upload
// @Tags brokers group
// @Accept json
// @Produce json
// @Param id path int true "ID"                 //TODO: MiddleWare'dan olish kerak
// @Param file formData file true "File"
// @Success 200 {object} models.BrokerResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UploadBrokersGroupImage(c *gin.Context) {
	var file File
	BrokerGroupId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})

		return
	}

	err = c.ShouldBind(&file)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	id := uuid.New()
	fileName := id.String() + filepath.Ext(file.File.Filename)
	dst, _ := os.Getwd()
	if _, err := os.Stat(dst + "/media"); os.IsNotExist(err) {
		os.Mkdir(dst+"/media", os.ModePerm)
	}

	filePath := "/media/" + fileName
	if err = c.SaveUploadedFile(file.File, dst+filePath); err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	brokersGroup, err := h.grpcClient.BrokersGroupService().UploadBrokersGroupImage(context.Background(), &pbbg.BrokersGroupImageUploadRequest{
		BrokersGroupId: int64(BrokerGroupId),
		ImageUrl:       filePath,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, models.BrokersGroupResponse{
		Id:        brokersGroup.Id,
		Name:      brokersGroup.Name,
		GroupInfo: brokersGroup.GroupInfo,
		AdminInfo: models.BrokerInfo{
			Id:              brokersGroup.AdminInfo.Id,
			FirstName:       brokersGroup.AdminInfo.FirstName,
			LastName:        brokersGroup.AdminInfo.LastName,
			Username:        brokersGroup.AdminInfo.Username,
			ProfileImageUrl: brokersGroup.AdminInfo.ProfileImageUrl,
			Email:           brokersGroup.AdminInfo.Email,
		},
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt,
	})
}

// @Router /brokers-group/update-level/{id} [post]
// @Summary Update a brokersGroup level
// @Description  Update a brokersGroup level
// @Tags brokers group
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param level query models.SetBrokerLevel false "Level"
// @Success 200 {object} models.BrokersGroupResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateBrokersGroupLevel(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	brokersGroup, err := h.grpcClient.BrokersGroupService().UploadBrokersGroupLevel(context.Background(), &pbbg.UploadBrokersGroupLevelRequest{
		BrokersGroupId: int64(id),
		Level:          ctx.Query("level"),
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, models.BrokersGroupResponse{
		Id:        brokersGroup.Id,
		Name:      brokersGroup.Name,
		GroupInfo: brokersGroup.GroupInfo,
		AdminInfo: models.BrokerInfo{
			Id:              brokersGroup.AdminInfo.Id,
			FirstName:       brokersGroup.AdminInfo.FirstName,
			LastName:        brokersGroup.AdminInfo.LastName,
			Username:        brokersGroup.AdminInfo.Username,
			ProfileImageUrl: brokersGroup.AdminInfo.ProfileImageUrl,
			Email:           brokersGroup.AdminInfo.Email,
		},
		PhoneNumber: brokersGroup.PhoneNumber,
		Email:       brokersGroup.Email,
		Address:     brokersGroup.Address,
		ImageUrl:    brokersGroup.ImageUrl,
		Level:       brokersGroup.Level,
		CreatedAt:   brokersGroup.CreatedAt,
	})
}

// @Router /brokers-group/delete/{id} [delete]
// @Summary Delete a Brokers Group
// @Description Delete a brokers Group
// @Tags brokers group
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteBrokersGroup(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 0)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})
		return
	}

	_, err = h.grpcClient.BrokersGroupService().DeleteBrokersGroup(context.Background(), &pbbg.BrokersGroupIdRequest{
		Id: id,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "successful delete method",
	})
}
