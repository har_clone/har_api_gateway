package v1

import (
	"context"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"

	"github.com/google/uuid"

	"gitlab.com/har_clone/har_api_gateway/api/models"
	pbu "gitlab.com/har_clone/har_api_gateway/genproto/user_service"
)

func parseUserModel(user *pbu.User) *models.ResponseUser {
	return &models.ResponseUser{
		ID:              user.Id,
		FirstName:       user.FirstName,
		LastName:        user.LastName,
		Username:        user.Username,
		ProfileImageUrl: user.ProfileImageUrl,
		Bio:             user.Bio,
		Email:           user.Email,
		Gender:          user.Gender,
		PhoneNumber:     user.PhoneNumber,
		Type:            user.Type,
		CreatedAt:       user.CreatedAt,
	}
}

// @Router /users/create [post]
// @Summary Create a user
// @Description Create a user
// @Tags users
// @Accept json
// @Produce json
// @Param user body models.CreateUserRequest true "User"
// @Success 201 {object} models.User
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		req models.CreateUserRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	user, err := h.grpcClient.UserService().CreateUser(context.Background(), &pbu.CreateUserRequest{
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    req.Username,
		Password:    req.Password,
		Email:       req.Email,
		PhoneNumber: req.PhoneNumber,
		Gender:      req.Gender,
		Type:        "user",
		Bio:         req.Bio,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, parseUserModel(user))
}

// @Router /users/get/{id} [get]
// @Summary Get user by id
// @Description Get user by id
// @Tags users
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.User
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetUser(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	user, err := h.grpcClient.UserService().GetUser(context.Background(), &pbu.UserIdRequest{Id: int64(id)})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, parseUserModel(user))
}

// @Router /users/get-all [get]
// @Summary Get all users
// @Description Get all users
// @Tags users
// @Accept json
// @Produce json
// @Param filter query models.GetAllUserParams false "Filter"
// @Success 200 {object} models.GetAllUsersResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllUsers(c *gin.Context) {
	req, err := validateGetAllParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.UserService().GetAllUsers(context.Background(), &pbu.GetAllUsersRequest{
		Page:       req.Page,
		Limit:      req.Limit,
		Search:     req.Search,
		SortByDate: req.SortByDate,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, getUsersResponse(result))
}

func getUsersResponse(data *pbu.GetAllUsersResponse) *models.GetAllUsersResponse {
	response := models.GetAllUsersResponse{
		Users: make([]*models.ResponseUser, 0),
		Count: data.Count,
	}

	for _, user := range data.Users {
		u := parseUserModel(user)
		response.Users = append(response.Users, u)
	}

	return &response
}

func validateGetAllParams(c *gin.Context) (*models.GetAllUserParams, error) {
	var (
		limit int = 10
		page  int = 1
		err   error
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllUserParams{
		Limit:      int32(limit),
		Page:       int32(page),
		Search:     c.Query("search"),
		SortByDate: c.Query("sort_by_date"),
	}, nil
}

// @Router /users/update/{id} [put]
// @Summary Update a user
// @Description Update a users
// @Tags users
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param user body models.UpdateUserRequest true "user"
// @Success 200 {object} models.User
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateUser(ctx *gin.Context) {
	var (
		req models.UpdateUserRequest
	)

	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	user, err := h.grpcClient.UserService().UpdateUser(context.Background(), &pbu.ChangeUser{
		Id:          int64(id),
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    req.Username,
		PhoneNumber: req.PhoneNumber,
		Gender:      req.Gender,
		Bio:         req.Bio,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, parseUserModel(user))
}

// @Router /users/delete/{id} [delete]
// @Summary Delete a User
// @Description Delete a user
// @Tags users
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteUser(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 0)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})
		return
	}

	_, err = h.grpcClient.UserService().DeleteUser(context.Background(), &pbu.UserIdRequest{
		Id: id,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, models.ResponseOK{
		Message: "successfull method",
	})
}

// @Router /users/image-upload/{id} [post]
// @Summary File image upload
// @Description File image upload
// @Tags users
// @Accept json
// @Produce json
// @Param id path int true "ID"                 //TODO: MiddleWare'dan olish kerak
// @Param file formData file true "File"
// @Success 200 {object} models.ResponseUser
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UserImageUpload(c *gin.Context) {
	var file File
	UserId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})

		return
	}

	err = c.ShouldBind(&file)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	id := uuid.New()
	fileName := id.String() + filepath.Ext(file.File.Filename)
	dst, _ := os.Getwd()
	if _, err := os.Stat(dst + "/media"); os.IsNotExist(err) {
		os.Mkdir(dst+"/media", os.ModePerm)
	}

	filePath := "/media/" + fileName
	if err = c.SaveUploadedFile(file.File, dst+filePath); err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	user, err := h.grpcClient.UserService().UserImageUpload(context.Background(), &pbu.UserImageUploadRequest{
		UserId:   int64(UserId),
		ImageUrl: filePath,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, parseUserModel(user))
}
