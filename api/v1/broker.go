package v1

import (
	"context"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	"gitlab.com/har_clone/har_api_gateway/api/models"
	pbb "gitlab.com/har_clone/har_api_gateway/genproto/broker_service"
)

type FiLe struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

// @Router /brokers/create [post]
// @Summary Create a broker
// @Description Create a broker
// @Tags broker
// @Accept json
// @Produce json
// @Param broker body models.CreateBrokerRequest true "Broker"
// @Success 201 {object} models.BrokerResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateBroker(c *gin.Context) {
	var (
		req models.CreateBrokerRequest
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	broker, err := h.grpcClient.BrokerService().CreateBroker(context.Background(), &pbb.CreateBrokerRequest{
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    req.Username,
		Password:    req.Password,
		Email:       req.Email,
		PhoneNumber: req.PhoneNumber,
		Gender:      req.Gender,
		Bio:         req.Bio,
		Type:        "broker",
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusCreated, models.BrokerResponse{
		Id:              broker.Id,
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,

		BrokersGroupInfo: models.BrokersGroupInfo{
			Id:          broker.BrokerGroupInfo.Id,
			Name:        broker.BrokerGroupInfo.Name,
			GroupInfo:   broker.BrokerGroupInfo.GroupInfo,
			PhoneNumber: broker.BrokerGroupInfo.PhoneNumber,
			Email:       broker.BrokerGroupInfo.Email,
			Address:     broker.BrokerGroupInfo.Address,
			ImageUrl:    broker.BrokerGroupInfo.ImageUrl,
			Level:       broker.BrokerGroupInfo.Level,
			CreatedAt:   broker.BrokerGroupInfo.CreatedAt,
		},
		Rating:    broker.Rating,
		Level:     broker.Level,
		CardId:    int(broker.CardId),
		CreatedAt: broker.CreatedAt,
	})
}

// @Router /brokers/get/{id} [get]
// @Summary Get broker by id
// @Description Get broker by id
// @Tags broker
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.BrokerResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetBroker(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	broker, err := h.grpcClient.BrokerService().GetBroker(context.Background(), &pbb.BrokerIdRequest{Id: int64(id)})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, models.BrokerResponse{
		Id:              broker.Id,
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,

		BrokersGroupInfo: models.BrokersGroupInfo{
			Id:          broker.BrokerGroupInfo.Id,
			Name:        broker.BrokerGroupInfo.Name,
			GroupInfo:   broker.BrokerGroupInfo.GroupInfo,
			PhoneNumber: broker.BrokerGroupInfo.PhoneNumber,
			Email:       broker.BrokerGroupInfo.Email,
			Address:     broker.BrokerGroupInfo.Address,
			ImageUrl:    broker.BrokerGroupInfo.ImageUrl,
			Level:       broker.BrokerGroupInfo.Level,
			CreatedAt:   broker.BrokerGroupInfo.CreatedAt,
		},
		Rating:    broker.Rating,
		Level:     broker.Level,
		CardId:    int(broker.CardId),
		CreatedAt: broker.CreatedAt,
	})
}

// @Router /brokers/get-all [get]
// @Summary Get all brokers
// @Description Get all brokers
// @Tags broker
// @Accept json
// @Produce json
// @Param filter query models.GetAllBrokerParams false "Filter"
// @Success 200 {object} models.GetAllBrokersResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllBrokers(c *gin.Context) {
	req, err := validateGetAllParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	result, err := h.grpcClient.BrokerService().GetAllBrokers(context.Background(), &pbb.GetAllBrokersRequest{
		Page:       req.Page,
		Limit:      req.Limit,
		Search:     req.Search,
		SortByDate: req.SortByDate,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, getBrokersResponse(result))
}

func getBrokersResponse(data *pbb.GetAllBrokersResponse) *models.GetAllBrokersResponse {
	response := models.GetAllBrokersResponse{
		Brokers: make([]*models.ResponseBroker, 0),
		Count:   data.Count,
	}

	for _, broker := range data.Brokers {
		response.Brokers = append(response.Brokers, &models.ResponseBroker{
			ID:              broker.Id,
			FirstName:       broker.FirstName,
			LastName:        broker.LastName,
			ProfileImageUrl: broker.ProfileImageUrl,
			Username:        broker.Username,
			Bio:             broker.Bio,
			Type:            broker.Type,
			Email:           broker.Email,
			Gender:          broker.Gender,
			PhoneNumber:     broker.PhoneNumber,
			BrokerGroupId:   broker.BrokerGroupId,
			Rating:          broker.Rating,
			Level:           broker.Level,
			CardId:          int(broker.CardId),
			CreatedAt:       broker.CreatedAt,
		})
	}

	return &response
}

// @Router /brokers/update/{id} [put]
// @Summary Update a broker
// @Description Update a brokers
// @Tags broker
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param broker body models.UpdateBrokerRequest true "broker"
// @Success 200 {object} models.BrokerResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateBroker(ctx *gin.Context) {
	var (
		req models.UpdateBrokerRequest
	)

	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	broker, err := h.grpcClient.BrokerService().UpdateBroker(context.Background(), &pbb.ChangeBroker{
		Id:          int64(id),
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    req.Username,
		PhoneNumber: req.PhoneNumber,
		Gender:      req.Gender,
		Bio:         req.Bio,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, models.BrokerResponse{
		Id:              broker.Id,
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,

		BrokersGroupInfo: models.BrokersGroupInfo{
			Id:          broker.BrokerGroupInfo.Id,
			Name:        broker.BrokerGroupInfo.Name,
			GroupInfo:   broker.BrokerGroupInfo.GroupInfo,
			PhoneNumber: broker.BrokerGroupInfo.PhoneNumber,
			Email:       broker.BrokerGroupInfo.Email,
			Address:     broker.BrokerGroupInfo.Address,
			ImageUrl:    broker.BrokerGroupInfo.ImageUrl,
			Level:       broker.BrokerGroupInfo.Level,
			CreatedAt:   broker.BrokerGroupInfo.CreatedAt,
		},
		Rating:    broker.Rating,
		Level:     broker.Level,
		CardId:    int(broker.CardId),
		CreatedAt: broker.CreatedAt,
	})
}

// @Router /brokers/add-card [put]
// @Summary Add broker card
// @Description Add broker card
// @Tags broker
// @Accept json
// @Produce json
// @Param broker body models.AddCardReq true "broker"
// @Success 200 {object} models.BrokerResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) AddCard(ctx *gin.Context) {
	var (
		req models.AddCardReq
	)

	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	broker, err := h.grpcClient.BrokerService().AddCard(context.Background(), &pbb.AddCardReq{
		BrokerId: req.BrokerId,
		CardId:   req.CardId,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, models.BrokerResponse{
		Id:              broker.Id,
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,

		BrokersGroupInfo: models.BrokersGroupInfo{
			Id:          broker.BrokerGroupInfo.Id,
			Name:        broker.BrokerGroupInfo.Name,
			GroupInfo:   broker.BrokerGroupInfo.GroupInfo,
			PhoneNumber: broker.BrokerGroupInfo.PhoneNumber,
			Email:       broker.BrokerGroupInfo.Email,
			Address:     broker.BrokerGroupInfo.Address,
			ImageUrl:    broker.BrokerGroupInfo.ImageUrl,
			Level:       broker.BrokerGroupInfo.Level,
			CreatedAt:   broker.BrokerGroupInfo.CreatedAt,
		},
		Rating:    broker.Rating,
		Level:     broker.Level,
		CardId:    int(broker.CardId),
		CreatedAt: broker.CreatedAt,
	})
}

// @Router /brokers/update-level/{id} [post]
// @Summary Update a broker's level
// @Description  Update a broker's level
// @Tags broker
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Param level query models.SetBrokerLevel false "Level"
// @Success 200 {object} models.BrokerResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) UpdateBrokerLevel(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	broker, err := h.grpcClient.BrokerService().UpdateBrokerLevel(context.Background(), &pbb.SetBrokerLevel{
		Id:    int64(id),
		Level: ctx.Query("level"),
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, models.BrokerResponse{
		Id:              broker.Id,
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,

		BrokersGroupInfo: models.BrokersGroupInfo{
			Id:          broker.BrokerGroupInfo.Id,
			Name:        broker.BrokerGroupInfo.Name,
			GroupInfo:   broker.BrokerGroupInfo.GroupInfo,
			PhoneNumber: broker.BrokerGroupInfo.PhoneNumber,
			Email:       broker.BrokerGroupInfo.Email,
			Address:     broker.BrokerGroupInfo.Address,
			ImageUrl:    broker.BrokerGroupInfo.ImageUrl,
			Level:       broker.BrokerGroupInfo.Level,
			CreatedAt:   broker.BrokerGroupInfo.CreatedAt,
		},
		Rating:    broker.Rating,
		Level:     broker.Level,
		CardId:    int(broker.CardId),
		CreatedAt: broker.CreatedAt,
	})
}

// @Router /brokers/image-upload/{id} [post]
// @Summary File image upload
// @Description File image upload
// @Tags broker
// @Accept json
// @Produce json
// @Param id path int true "ID"                 //TODO: MiddleWare'dan olish kerak
// @Param file formData file true "File"
// @Success 200 {object} models.BrokerResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) BrokerImageUpload(c *gin.Context) {
	var file File

	BrokerId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})

		return
	}

	err = c.ShouldBind(&file)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	id := uuid.New()
	fileName := id.String() + filepath.Ext(file.File.Filename)
	dst, _ := os.Getwd()
	if _, err := os.Stat(dst + "/media"); os.IsNotExist(err) {
		os.Mkdir(dst+"/media", os.ModePerm)
	}

	filePath := "/media/" + fileName
	if err = c.SaveUploadedFile(file.File, dst+filePath); err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	broker, err := h.grpcClient.BrokerService().BrokerImageUpload(context.Background(), &pbb.BrokerImageUploadReq{
		BrokerId: int64(BrokerId),
		ImageUrl: filePath,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, models.BrokerResponse{
		Id:              broker.Id,
		FirstName:       broker.FirstName,
		LastName:        broker.LastName,
		Username:        broker.Username,
		ProfileImageUrl: broker.ProfileImageUrl,
		Bio:             broker.Bio,
		Email:           broker.Email,
		Gender:          broker.Gender,
		PhoneNumber:     broker.PhoneNumber,
		Type:            broker.Type,

		BrokersGroupInfo: models.BrokersGroupInfo{
			Id:          broker.BrokerGroupInfo.Id,
			Name:        broker.BrokerGroupInfo.Name,
			GroupInfo:   broker.BrokerGroupInfo.GroupInfo,
			PhoneNumber: broker.BrokerGroupInfo.PhoneNumber,
			Email:       broker.BrokerGroupInfo.Email,
			Address:     broker.BrokerGroupInfo.Address,
			ImageUrl:    broker.BrokerGroupInfo.ImageUrl,
			Level:       broker.BrokerGroupInfo.Level,
			CreatedAt:   broker.BrokerGroupInfo.CreatedAt,
		},
		Rating:    broker.Rating,
		Level:     broker.Level,
		CardId:    int(broker.CardId),
		CreatedAt: broker.CreatedAt,
	})
}

// @Router /brokers/delete/{id} [delete]
// @Summary Delete a Broker
// @Description Delete a broker
// @Tags broker
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) DeleteBroker(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 0)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})
		return
	}

	_, err = h.grpcClient.BrokerService().DeleteBroker(context.Background(), &pbb.BrokerIdRequest{
		Id: id,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "successful delete method",
	})
}
