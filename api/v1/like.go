package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/har_clone/har_api_gateway/api/models"
	pb "gitlab.com/har_clone/har_api_gateway/genproto/comment_service"
)

// @Router /likes/create-update [post]
// @Summary Create or update like
// @Description Create or update like
// @Tags like
// @Accept json
// @Produce json
// @Param filter query models.CreateOrUpdateLikeRequest true "Filter"
// @Success 200 {object} models.ResponseOK
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateOrUpdateLike(c *gin.Context) {
	var (
		UserId  int
		GuestId int
		Status  bool
		err     error
	)

	if c.Query("user_id") != "" {
		UserId, err = strconv.Atoi(c.Query("user_id"))
		if err != nil {
			return
		}
	}
	if c.Query("guest_id") != "" {
		GuestId, err = strconv.Atoi(c.Query("guest_id"))
		if err != nil {
			return
		}
	}

	if c.Query("status") != "" {
		Status, err = strconv.ParseBool(c.Query("user_id"))
		if err != nil {
			return
		}
	}

	// TODO: MiddleWare

	_, err = h.grpcClient.LikeService().CreateOrUpdate(context.Background(), &pb.Like{
		UserId:   int64(UserId),
		GuestId:  int64(GuestId),
		Status:   Status,
		LikeType: c.Query("like_type"),
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	c.JSON(http.StatusOK, models.ResponseOK{
		Message: "DONE",
	})
}

// @Router /likes/like-info [get]
// @Summary Get like info
// @Description Get like info
// @Tags like
// @Accept json
// @Produce json
// @Param like query models.GetLikeInfo true "like"
// @Success 200 {object} models.Like
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetLike(c *gin.Context) {
	var (
		UserId  int
		GuestId int
		err     error
	)

	if c.Query("user_id") != "" {
		UserId, err = strconv.Atoi(c.Query("user_id"))
		if err != nil {
			return
		}
	}
	if c.Query("guest_id") != "" {
		GuestId, err = strconv.Atoi(c.Query("guest_id"))
		if err != nil {
			return
		}
	}
	// TODO: MiddleWare
	resp, err := h.grpcClient.LikeService().Get(context.Background(), &pb.GetLikeInfo{
		LikeType: c.Query("like_type"),
		GuestId:  int64(GuestId),
		UserId:   int64(UserId),
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
	}
	c.JSON(http.StatusOK, models.Like{
		Id:      resp.Id,
		GuestId: resp.GuestId,
		UserId:  resp.UserId,
		Status:  resp.Status,
	})
}

// @Router /likes/likes-dislikes [get]
// @Summary Get like and dislike
// @Description Get like and dislike
// @Tags like
// @Accept json
// @Produce json
// @Param like query models.IdLikeRequest true "like"
// @Success 200 {object} models.LikesDislikesCountsResult
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetLikesDislikes(c *gin.Context) {

	var (
		GuestId int
		err     error
	)

	if c.Query("guest_id") != "" {
		GuestId, err = strconv.Atoi(c.Query("guest_id"))
		if err != nil {
			return
		}
	}
	// TODO: MiddleWare

	resp, err := h.grpcClient.LikeService().GetLikesDislikesCount(context.Background(), &pb.IdLikeRequest{
		LikeType: c.Query("like_type"),
		Id:       int64(GuestId),
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResponse(err))
	}
	c.JSON(http.StatusOK, models.LikesDislikesCountsResult{
		LikesCount:    resp.LikesCount,
		DislikesCount: resp.DislikesCount,
	})
}
