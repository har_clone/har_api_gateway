package api

import (
	"github.com/gin-gonic/gin"
	v1 "gitlab.com/har_clone/har_api_gateway/api/v1"
	"gitlab.com/har_clone/har_api_gateway/config"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	_ "gitlab.com/har_clone/har_api_gateway/api/docs" // for swagger

	grpcPkg "gitlab.com/har_clone/har_api_gateway/pkg/grpc_client"
)

type RouterOptions struct {
	Cfg        *config.Config
	GrpcClient grpcPkg.GrpcClientI
}

// @title           Swagger for blog api
// @version         1.0
// @description     This is a blog service api.
// @host      localhost:8000
// @BasePath  /v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

func New(opt *RouterOptions) *gin.Engine {
	router := gin.Default()

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Cfg:        opt.Cfg,
		GrpcClient: &opt.GrpcClient,
	})

	apiV1 := router.Group("/v1")

	// User
	apiV1.POST("/users/create", handlerV1.CreateUser)
	apiV1.GET("/users/get/:id", handlerV1.GetUser)
	apiV1.GET("/users/get-all", handlerV1.GetAllUsers)
	apiV1.PUT("/users/update/:id", handlerV1.UpdateUser)
	apiV1.DELETE("/users/delete/:id", handlerV1.DeleteUser)
	apiV1.POST("/users/image-upload/:id", handlerV1.UserImageUpload)

	// // Category
	// apiV1.GET("/categories", handlerV1.GetAllCategory)
	// apiV1.GET("/categories/:id", handlerV1.GetCategory)
	// apiV1.POST("/categories", handlerV1.CreateCategory)
	// apiV1.PUT("/categories/:id", handlerV1.UpdateCategory)
	// apiV1.DELETE("/categories/:id", handlerV1.DeleteCategory)

	// Broker
	apiV1.GET("/brokers/get-all", handlerV1.GetAllBrokers)
	apiV1.GET("/brokers/get/:id", handlerV1.GetBroker)
	apiV1.POST("/brokers/create", handlerV1.CreateBroker)
	apiV1.PUT("/brokers/update/:id", handlerV1.UpdateBroker)
	apiV1.POST("/brokers/update-level/:id", handlerV1.UpdateBrokerLevel)
	apiV1.POST("/brokers/image-upload/:id", handlerV1.BrokerImageUpload)
	apiV1.PUT("/brokers/add-card", handlerV1.AddCard)
	apiV1.DELETE("/brokers/delete/:id", handlerV1.DeleteBroker)

	// Broker Group
	apiV1.GET("/brokers-group/get-all", handlerV1.GetAllBrokersGroups)
	apiV1.GET("/brokers-group/get/:id", handlerV1.GetBrokersGroup)
	apiV1.POST("/brokers-group/create", handlerV1.CreateBrokersGroup)
	apiV1.PUT("/brokers-group/update/:id", handlerV1.UpdateBrokersGroup)
	apiV1.POST("/brokers-group/update-level/:id", handlerV1.UpdateBrokersGroupLevel)
	apiV1.POST("/brokers-group/image-upload/:id", handlerV1.UploadBrokersGroupImage)
	apiV1.POST("/brokers-group/add-member", handlerV1.AddMember)
	apiV1.GET("/brokers-group/get-members/:id", handlerV1.GetMembers)
	apiV1.DELETE("/brokers-group/delete/:id", handlerV1.DeleteBrokersGroup)

	// // Apartment
	// apiV1.GET("/apartments", handlerV1.ApartmentGetAll)
	// apiV1.GET("/apartments/:id", handlerV1.ApartmentGet)
	// apiV1.POST("/apartments", handlerV1.ApartmentCreate)
	// apiV1.PUT("/apartments/:id", handlerV1.ApartmentUpdate)
	// apiV1.DELETE("/apartments/:id", handlerV1.ApartmentDelete)

	// // Register
	// apiV1.POST("/auth/register", handlerV1.Register)
	// apiV1.POST("/auth/verify", handlerV1.Verify)
	// apiV1.POST("/auth/login", handlerV1.Login)
	// apiV1.POST("/auth/forgot-password", handlerV1.ForgotPassword)
	// apiV1.POST("/auth/verify-forgot-password", handlerV1.VerifyForgotPassword)
	// apiV1.POST("/auth/update-password", handlerV1.UpdatePassword)

	// Comment
	apiV1.GET("/comments/get-all", handlerV1.GetAllComment)
	apiV1.GET("/comments/get/:id", handlerV1.GetComment)
	apiV1.POST("/comments/create", handlerV1.CreateComment)
	apiV1.PUT("/comments/update/:id", handlerV1.UpdateComment)
	apiV1.DELETE("/comments/delete/:id", handlerV1.DeleteComment)

	// Like
	apiV1.POST("/likes/create-update", handlerV1.CreateOrUpdateLike)
	apiV1.GET("/likes/like-info", handlerV1.GetLike)
	apiV1.GET("/likes/likes-dislikes", handlerV1.GetLikesDislikes)

	// Collected Apartment
	apiV1.POST("/collected-apartments/add", handlerV1.AddCollectApartment)
	apiV1.GET("collected-apartments/get/:id", handlerV1.GetCollectedApartment)
	apiV1.GET("collected-apartments/get-all", handlerV1.GetAllCollectedApartments)
	apiV1.DELETE("collected-apartments/delete/:id", handlerV1.DeleteCollectedApartment)

	// Payment Service
	apiV1.POST("/payment/link", handlerV1.CreatePaymentLink)
	apiV1.POST("/payment/stripe-webhook", handlerV1.StripeWebhook)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
